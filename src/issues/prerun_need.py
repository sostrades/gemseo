from numpy import array
from gemseo.disciplines.auto_py import AutoPyDiscipline
from gemseo.mda.mda_chain import MDAChain
import traceback

def test_execute(disc, inputs):
    try:
        disc.execute(inputs)
    except Exception as e:
        print('\t\t' + str(traceback.format_exc()))

def reload_case():
    '''
    disciplines & MDA definition 
    '''
    # discipline 1
    def d1_f(shared_2):
        shared_1 = array([(shared_2[0] + shared_2[1]) / 2.])
        return shared_1
    d1 = AutoPyDiscipline(d1_f)
    
    # discipline 2
    def d2_f(shared_1):
        shared_2 = array([0.5 * (shared_1 + 1. / (2 * shared_1))]*2)
        return shared_2
    d2 = AutoPyDiscipline(d2_f)
    
    mda = MDAChain([d1, d2])
    
    return d1, d2, mda

''' 
definition of all inputs is :
'''
d1_inputs = {'shared_2': array([8., 9.])}
d2_inputs = {'shared_1': array([5.])}
inputs = {**d1_inputs,**d2_inputs}
d1, d2, mda = reload_case()
inputs_by_disc = {d1.name:d1_inputs, 
                  d2.name:d2_inputs}

'''
A run of the MDA shows that it works when we pass the inputs of all disciplines
'''
test_execute(mda, inputs)
 
'''
However, if we want to pass the strict necessary information, let say the input of the first MDA discipline, it will fail:
'''
d1, d2, mda = reload_case()
# we get the discipline execution order
disc_order = [d for d in mda.mdo_chain.disciplines[0].disciplines]
# we get the first discipline inputs only and execute the MDA
first_disc_inputs = inputs_by_disc[disc_order[0].name]

print("\n")
print("Discipline execution order is ", [d.name for d in disc_order])
print("Althouth only inputs of first discipline <%s> should be required to run," %str(disc_order[0].name))
print("it fails if we provide only the first discipline inputs %s :"%str(first_disc_inputs))
test_execute(mda, first_disc_inputs)
 
'''
If we deactivate the check input data, the execution succeeds. 
(meaning that in this case, the second discipline inputs are only required by the check input method, not for the actual discipline execution)
We will keep this flag deactivated for all other cases to see where are the crashes at execution.
'''
 
from gemseo.api import configure
# check input data deactivation
configure(check_input_data=False)
print("\n")
print("Discipline execution order is ", [d.name for d in disc_order])
print("it does not fail if we provide only the first discipline inputs %s :"%str(first_disc_inputs))
print("AND we deactivate the check input flag")
d1, d2, mda = reload_case()
test_execute(mda, first_disc_inputs)

'''
Now, let say that the inputs of discipline 1 (first to run) are unknown by the user.
We want thus to be able to execute the MDA,
'''
# we get the discipline execution order
d1, d2, mda = reload_case()
disc_order = [d for d in mda.mdo_chain.disciplines[0].disciplines]
# we get the second discipline inputs only and execute the MDA
second_disc_inputs = inputs_by_disc[disc_order[1].name]
print("\n")
print("Discipline execution order is ", [d.name for d in disc_order])
print("it fails if we provide only the second discipline inputs %s :"%str(second_disc_inputs))

test_execute(mda, second_disc_inputs)
