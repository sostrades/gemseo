# -*- coding: utf-8 -*-
# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# Contributors:
#    INITIAL AUTHORS - API and implementation and/or documentation
#        :author: Francois Gallard
#    OTHER AUTHORS   - MACROSCOPIC CHANGES

"""A factory to instantiate linear solvers from their class names."""

from __future__ import division, unicode_literals

import logging
from typing import Any, List

from numpy import ndarray

from gemseo.algos.driver_factory import DriverFactory
from gemseo.algos.linear_solvers.linear_problem import LinearProblem
from gemseo.algos.linear_solvers.linear_solver_lib import LinearSolverLib

LOGGER = logging.getLogger(__name__)


class LinearSolversFactory(DriverFactory):
    """MDA factory to create the MDA from a name or a class."""

    def __init__(self):  # type: (...) -> None
        super(LinearSolversFactory, self).__init__(
            LinearSolverLib, "gemseo.algos.linear_solvers"
        )

    @property
    def linear_solvers(self):  # type: (...) -> List[str]
        """Return the available classes names.

        Returns:
             The names of the classes.
        """
        return self.factory.classes

    def is_available(
        self, solver_name  # type: str
    ):
        """Check the availability of a LinearSolver.

        Args:
            solver_name: The name of the LinearSolver.

        Returns:
            Whether the LinearSolver is available.
        """
        return super(LinearSolversFactory, self).is_available(solver_name)

    def create_lib(self, algo_name):
        """Create the lib with a given algo_name

        Args:
            algo_name: The name of the algorithm.

        Returns:
           the library instantiated.

           SOSTRADES modif : use to create the  lib outside of the execution 
           to use the same lib for multiple succesive execution 
        """
        lib = self.create(algo_name)
        return lib

    def execute(
        self,
        problem,  # type: LinearProblem
        algo_name,
        check_and_options=False,
        lib=None,  # type: str
        **options  # type: Any
    ):  # type: (...) -> ndarray
        """Execute the driver.

        Find the appropriate library and execute the driver on the problem to solve
        the linear system LHS.x = RHS.

        Args:
            problem: The linear equations and right hand side
             (lhs, rhs) that defines the linear problem. XXX is a tuple expected?
            algo_name: The algorithm name.
            options: The options for the algorithm,
                see associated JSON file.

        Returns:
            The solution.
        """
        if lib is None:
            lib = self.create_lib(algo_name)

        return lib.execute(problem, algo_name=algo_name, check_and_options=check_and_options, **options)
